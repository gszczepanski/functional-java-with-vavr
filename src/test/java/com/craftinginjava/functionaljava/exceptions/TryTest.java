package com.craftinginjava.functionaljava.exceptions;

import io.vavr.control.Try;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.omg.SendingContext.RunTime;

import java.io.IOException;

import static io.vavr.API.Try;
import static org.assertj.core.api.Assertions.assertThat;

public class TryTest {

    @Test
    public void shouldCatchExceptionAsJavaObj() {
        Try<String> aTry = Try(() -> fetchCustomerNameFromDatabase());

        assertThat(aTry.isFailure()).isTrue();
    }

    @Test
    public void weCanDefineCallbackAndFallbackForOperation() {
        Try(() -> fetchCustomerNameFromDatabase())
                .onSuccess(this::handleCustomerNameSuccess)
                .onFailure(this::handleCustomerNameFailure);
    }

    @Test(expected = RuntimeException.class)
    public void weCanDefineDifferentExceptionOnIOExceptionCause() {

        Try(() -> fetchCustomerNameFromDatabase())
                .getOrElseThrow(e -> new RuntimeException(e));
    }

    @Test
    public void weCanCombineChainOfTryFunctions() {
        Try(() -> fetchCustomerNameFromDatabase())
                .andThenTry(customerName -> sendCustomerNameToRemoteService(customerName))
                .onSuccess(this::handleCustomerNameSuccess)
                .onFailure(this::handleCustomerNameFailure);
    }

    private String fetchCustomerNameFromDatabase() throws IOException {
        throw new IOException("Bad exception :<");
    }

    private Long sendCustomerNameToRemoteService(String customerName) throws IOException {
        System.out.println("Sending customer name to remote service: " + customerName);
        return 1L;
    }

    private void handleCustomerNameSuccess(String customerName) {
        System.out.println("Customer name is: " + customerName);
    }


    private void handleCustomerNameFailure(Throwable e) {
        System.err.println(e);
    }

}
