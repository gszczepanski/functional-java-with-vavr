package com.craftinginjava.functionaljava.collections;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Value;
import org.junit.Test;

import static io.vavr.API.List;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * GroupBy method takes function f(a) => b,
 * where a is element of the Collection, and b is classifier - the key of the returned map.
 *
 * The groupBy method takes a predicate function as
 * its parameter and uses it to group elements by key and values into a Map collection.
 */
public class GroupByTest {

    @Test
    public void shouldGropuOrdersByCountry() {
        Map<String, List<Order>> groupedOrders = jonOrders()
                .groupBy(o -> o.country);

        assertThat(groupedOrders.get("PL").get())
                .isEqualTo(
                        List(new Order("PL", "Product1"))
                );

        assertThat(groupedOrders.get("GB").get())
                .isEqualTo(
                        List(
                                new Order("GB", "Product2"),
                                new Order("GB", "Product4")
                        )
                );

        assertThat(groupedOrders.get("FR").get())
                .isEqualTo(
                        List(
                                new Order("FR", "Product3"),
                                new Order("FR", "Product5")
                        )
                );
    }

    private List<Order> jonOrders() {
        return List(
                new Order("PL", "Product1"),
                new Order("GB", "Product2"),
                new Order("FR", "Product3"),
                new Order("GB", "Product4"),
                new Order("FR", "Product5")
        );
    }

    @Value
    static class Order {

        private final String country;
        private final String name;

    }

}
