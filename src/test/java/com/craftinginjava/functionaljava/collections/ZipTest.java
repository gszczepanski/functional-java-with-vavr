package com.craftinginjava.functionaljava.collections;

import io.vavr.API;
import io.vavr.Function2;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.Value;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

import static io.vavr.API.List;
import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * The zip method takes another collection as parameter and will merge its elements
 * with the elements of the current collection to create a new collection consisting
 * of pairs or Tuple elements from both collections.
 *
 */
public class ZipTest {

    /**
     * Let's say that we have orders from one microservice,
     * and we want to merge them with product definitions from another microservice.
     *
     * The result will be invoice positions with data merged from two collections.
     */
    @Test
    public void shouldMergeOrderWithProductDefinition() {
        List<Tuple2<Order, Product>> zipped = maryOrders()
                .zip(productsDefinitions());

        List<InvoicePosition> invoicePositions = zipped
                .map(data -> new InvoicePosition(
                        data._1().productCode,
                        data._2().name,
                        data._1().price
                ));

        assertThat(invoicePositions)
                .isEqualTo(
                        List(
                            new InvoicePosition("p-001", "Oven", new BigDecimal("330.00")),
                            new InvoicePosition("p-002", "TV", new BigDecimal("1200.00")),
                            new InvoicePosition("p-003", "Microwave", new BigDecimal("499.99"))
                        )
                );
    }

    /**
     * ZipWith method is almost the same as zip method.
     *
     * The only difference is that on zipWith we also provide mapping function f(a, b) => c,
     * so the result is List of mapped c elements.
     */
    @Test
    public void shouldMergeOrderWithProductDefinitionUsingZipWith() {
        Function2<Order, Product, InvoicePosition> invoicePositionMappingFunction =
                (order, product) -> new InvoicePosition(order.productCode, product.name, order.price);

        List<InvoicePosition> invoicePositions = maryOrders()
                .zipWith(productsDefinitions(), invoicePositionMappingFunction);

        assertThat(invoicePositions)
                .isEqualTo(
                        List(
                                new InvoicePosition("p-001", "Oven", new BigDecimal("330.00")),
                                new InvoicePosition("p-002", "TV", new BigDecimal("1200.00")),
                                new InvoicePosition("p-003", "Microwave", new BigDecimal("499.99"))
                        )
                );
    }

    private List<Order> maryOrders() {
        return List(
                new Order("p-001", new BigDecimal("330.00")),
                new Order("p-002", new BigDecimal("1200.00")),
                new Order("p-003", new BigDecimal("499.99"))
        );
    }

    private List<Product> productsDefinitions() {
        return List(
                new Product("p-001", "Oven"),
                new Product("p-002", "TV"),
                new Product("p-003", "Microwave")
        );
    }

    @Value
    static class Order {

        private final String productCode;
        private final BigDecimal price;

    }

    @Value
    static class Product {

        private final String productCode;
        private final String name;

    }

    @Value
    static class InvoicePosition {

        private final String productCode;
        private final String name;
        private final BigDecimal price;

    }
}
