package com.craftinginjava.functionaljava.collections;

import io.vavr.Function2;
import io.vavr.collection.List;
import lombok.Value;
import org.junit.Test;

import java.math.BigDecimal;

import static io.vavr.API.List;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Experimenting with reduce function over List collection.
 * <p>
 * The reduce method takes an associative binary function as parameter and will use it to collapse elements from the collection.
 * <p>
 * So if we have List(e1, e2, e3, e4), and we define function f(a, b) => a + b, then reduce method will do following steps:
 * <p>
 * 1. f(e1, e2)
 * 2. f(f(e1, e2), e3)
 * 3. f(f(f(e1, e2), e3), e4)
 * <p>
 * and return result of this recursive f(a, b) invocation.
 */
public class ReduceTest {

    /**
     * Let's find Jons total price for ordered products.
     */
    @Test
    public void findTotalPriceForJonOrders() {
        BigDecimal totalPrice = jonOrders()
                .map(o -> o.price)
                .reduce((o1, o2) -> o1.add(o2));

        assertThat(totalPrice)
                .isEqualTo(new BigDecimal("15098.99"));
    }

    /**
     * Let's find the cheapest Jons order
     */
    @Test
    public void findCheapestOrder() {
        Order cheapestOrder = jonOrders()
                .reduce((o1, o2) -> o1.price.compareTo(o2.price) == -1 ? o1 : o2);

        assertThat(cheapestOrder.price)
                .isEqualTo(new BigDecimal("299.99"));
    }

    @Test
    public void findMostExpensiveOrder() {
        Function2<Order, Order, Order> findMostExpensiveOrder = (o1, o2) -> o1.price.compareTo(o2.price) == 1 ? o1 : o2;

        Order mostExpensiveOrder = jonOrders()
                .reduce(findMostExpensiveOrder);

        assertThat(mostExpensiveOrder.price)
                .isEqualTo(new BigDecimal("10000.00"));
    }

    private List<Order> jonOrders() {
        return List(
                new Order("TV", new BigDecimal("10000.00")),
                new Order("Microwave", new BigDecimal("299.99")),
                new Order("Fridge", new BigDecimal("2300.00")),
                new Order("Oven", new BigDecimal("2499.00"))
        );
    }

    @Value
    static class Order {

        private final String itemName;
        private final BigDecimal price;

    }


}
