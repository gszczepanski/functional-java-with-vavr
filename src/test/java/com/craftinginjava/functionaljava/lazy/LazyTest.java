package com.craftinginjava.functionaljava.lazy;

import io.vavr.Lazy;
import org.junit.Test;

import static io.vavr.API.Lazy;
import static org.assertj.core.api.Assertions.assertThat;

public class LazyTest {

    @Test
    public void shouldFetchLazyValueOnlyOnce() {
        Lazy<String> lazyRemoteCall = Lazy(() -> remoteHttpCall());

        System.out.println("Fetching value for the first time");
        String val = lazyRemoteCall.get();

        System.out.println("Fetching value for the second time");
        String val2 = lazyRemoteCall.get();

        assertThat(val).isEqualTo("500");
        assertThat(val2).isEqualTo("500");
    }

    private String remoteHttpCall() {
        System.out.println("Request send");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Response arrived");
        return "500";
    }

}
